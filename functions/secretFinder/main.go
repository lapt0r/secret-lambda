package main

import (
	"context"
	"fmt"
	"math"
	"regexp"

	"github.com/aws/aws-lambda-go/lambda"
	fuzzysplitter "gitlab.com/lapt0r/fuzzy-splitter"
)

// Payload is the input type for the lambda.  Requires an Identifier and a Content field
type Payload struct {
	Identifier string `json:"identifier"`
	Content    string `json:"content"`
}

// ScanResult is the return type for the lambda.  Returns a JSON object containing a boolean value for containsSecret
type ScanResult struct {
	ContainsSecret bool `json:"containsSecret"`
}

func getCharacterCount(text string) map[rune]int {
	var result = make(map[rune]int)
	for _, char := range text {
		var count = result[char]
		count++
		result[char] = count
	}
	return result
}

func getPValues(text string) []float64 {
	var charmap = getCharacterCount(text)
	var totalcharcount int = len(text)
	var result = make([]float64, len(charmap))

	var index = 0
	for _, count := range charmap {
		var pVal = float64(count) / float64(totalcharcount)
		result[index] = pVal
		index++
	}
	return result
}

//GetShannonEntropy gets the Shannon entropy of a string as a float.
func GetShannonEntropy(text string) float64 {
	var pvals = getPValues(text)
	var entropy = float64(0)
	for _, pVal := range pvals {
		entropy -= (pVal * math.Log(pVal))
	}
	return entropy
}

//GetMetricEntropy gets a normalized entropy between 0 and 1.
func GetMetricEntropy(text string) float64 {
	return GetShannonEntropy(text) / math.Log(float64(len(text)))
}

//HasKeyword returns whether or not the text blob contains a secret keyword
func HasKeyword(text string) bool {
	re := regexp.MustCompile("(?i)(pass(word)?|secret|credential|[a-z_-]*(token|key))")
	return re.MatchString(text)
}

//HasHighEntropyFragment returns whether or not the string contains a high entropy string fragment
func HasHighEntropyFragment(text string) bool {
	fragments := fuzzysplitter.FuzzySplit(text)
	for _, fragment := range fragments {
		if len(fragment) < 8 {
			//ignore short fragments, these are very unlikely to be credentials and will skew towards very high entropy
			continue
		}
		fragmentEntropy := GetMetricEntropy(fragment)
		fmt.Println(fragmentEntropy)
		if fragmentEntropy > 0.73 {
			return true
		}
	}
	return false
}

func handler(ctx context.Context, event Payload) (ScanResult, error) {
	// Lambda function for determining if a provided text contains a potentially secret value

	// Parameters
	// ----------
	// event: Payload, required
	//     Input event to the Lambda function

	// Returns
	// ------
	//     ScanResult: object containing the result of the secret scan

	// Get the string to be scanned from input
	content := event.Content
	hasSecret := HasKeyword(content)
	hasHighEntropyFragment := false
	if hasSecret {
		//this is expensive(ish) as it requires another string scan + some expensive slice operations
		hasHighEntropyFragment = HasHighEntropyFragment(content)
	}
	containsSecret := HasKeyword(content) && hasHighEntropyFragment
	return ScanResult{
		ContainsSecret: containsSecret,
	}, nil
}

func main() {
	lambda.Start(handler)
}
