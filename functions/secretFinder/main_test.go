package main

import (
	"context"
	"testing"
)

func TestHandler(t *testing.T) {
	t.Run("Check response properties", func(t *testing.T) {
		context := context.Background()
		event := Payload{
			Identifier: "foo",
			Content:    "secret = abcdefgh",
		}
		response, err := handler(context, event)
		if err != nil {
			t.Errorf("%s", err)
		}
		if !response.ContainsSecret {
			t.Errorf("Expected payload to contain secret.")
		}
	})
}

func TestHasKeyword(t *testing.T) {
	t.Run("Check positive keyword cases", func(t *testing.T) {
		testCases := map[string]bool{
			"password=\"foobarbiz\"": true,
			"foobarbiz":              false,
		}
		for text, result := range testCases {
			hasKeyword := HasKeyword(text)
			if hasKeyword != result {
				t.Errorf("expected hasKeyword for [%s] to be [%t] but was [%t]", text, result, hasKeyword)
			}
		}
	})
}
