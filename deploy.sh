#!/bin/bash

while getopts ":b:n:p:" opt; do
    case $opt in
        b) bucket_name="$OPTARG"
        ;;
        n) app_name="$OPTARG"
        ;;
        p) profile_name="$OPTARG"
        ;;
        \?) echo "INVALID OPTION -$OPTARG" >&2
        ;;
    esac
done

if [ -z "$bucket_name" ]
    then
        echo "Bucket name not set" >&2;
        return 1;
fi
if [ -z "$profile_name" ]
    then
        echo "profile name not set" >&2;
        return 1;
fi
if [ -z "$app_name" ]
    then
        echo "app name not set" >&2;
        return 1;
fi

echo "parameters validated.  deploying app [$app_name] to bucket [$bucket_name] using profile [$profile_name]"

echo "building SAM application.."
sam build
echo "packaging SAM application.."
sam package --output-template-file packaged.yaml --s3-bucket "$bucket_name" --profile "$profile_name"
echo "deploying SAM package to AWS.."
aws cloudformation deploy --template-file packaged.yaml --stack-name "$app_name" --capabilities CAPABILITY_IAM --profile "$profile_name"